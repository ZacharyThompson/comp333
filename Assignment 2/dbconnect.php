<?php
/**
 * This file contains a simple PDO connection instantiation, connecting to ziet1.
 *
 * @author Zachary Thompson (1287280)
 */
try{
	$con = new PDO('mysql:host=mysql.cms.waikato.ac.nz;dbname=ziet1','ziet1','my10823265sql');
} catch (PDOException $e) {
	echo "Database connection error " . $e->getMessage();
}
