/**
 * Some basic prototyping on the Array Object to allow explicit iteration over
 *  a list Object and removal of a given item.
 *
 */
Array.prototype.forEach = function(action) {
    for (let i = 0; i < this.length; i++) {
        action(this[i]);
    }
};

Array.prototype.remove = function(element) {
	let index = this.indexOf(element);
	if (index > -1) this.splice(index, 1);
};

/**
 * Basic encapsulation of an Ajax request that handles generic request cases.
 *
 * @param {String} Method : The HTTP request method to be used.
 * @param {String} URL : The url to send the XHR request to.
 * @param {Boolean} Async : Whether the XHR request is asynchronous.
 * @param {String} Data : The body of data to be sent in the XHR request.
 * @param {Function} onSuccess : The function to be invoked if the XHR request
 *    was sucessful.
 * @param {Function} onError : The function to be invoked if the XHR request
 *    was not successful.
 *
 */
function ajaxRequest(method, url, async, data, onSuccess, onError) {
	let request = new XMLHttpRequest();
	request.open(method, url, async);
	
	if (method === 'POST') {
		request.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	}

	request.onreadystatechange = function() {
		if (request.readyState === 4) {
			if (request.status === 200) {
				if (onSuccess != null) onSuccess(request);
			} else {
				if (onError != null) onError(request);
			}
		}
	};

	request.send(data);
}

/**
 * Constructor function for a ShareWidget Object.
 *
 *  The ShareWidget Object allows the user to monitor the share prices and change of a
 *   set of companies. 
 *  The widget uses a dropdown menu to list the companies that can be monitored. 
 *   Once selected, the company is added to a data list that displays the information which
 *   can then be updated using the update button or sorted using the sort dropdown menu.
 *
 * @param {HTMLElement} container_element : a DOM element inside which the widget will place its UI
 *
 */
function ShareWidget(container_element){
	// Object literal representing the widget's UI elements
	var _ui = {     
		container : null,
		companies : null,
		update : null,
		sort : null,
		options : null,
		data : null
	};
	
	/**
	 * Constructor function for a ShareLine Object.
	 *
	 *  The ShareLine Object stores the name, share price and share change for
	 *   a company that can be monitored using the encasing ShareWidget Object.   
	 *  The Object implements a getDomElement global function that is used to
	 *   retrieve the DOM element that is used to represent the ShareLine Object
	 *   in HTML.
	 *
	 * @param {String} s_company : The name of the company the Object represents.
	 * @param {Float} s_price : The share price of the company the Object represents.
	 * @param {Float} s_change : The share price of the company the Object represents.
	 * 
	 */
	var ShareLine = function(s_company, s_price, s_change) {
		// Private variables containing the company's share information
		var _company = s_company;
		var _price = s_price;
		var _change = s_change;		
						
		// Object literal representing the Object's UI elements
		var _ui = {
			container : null,			
			company : null,
			price : null,
			change : null
		};
		
		// Private function to construct the DOM elements needed to display the Object.
		var _createUI = function() {
			_ui.container = new DomElement('li', 'shareLine', '');
			_ui.company = new Container('', _company);
			_ui.price  = new Container('', _price);
			_ui.change = new Container('', _change);
			
			// Create and initialise each of the UI elements and add them to the _ui object
			_ui.container.appendChild(_ui.company, _ui.price, _ui.change);
		};
		
		// Getters and Setters
		this.getPrice = () => _price;
		this.getChange = () => _change;
		this.getCompany = () => _company;
		this.getDomElement = () => _ui.container;
		this.onclick = event => _ui.container.setOnClick(event);
		this.setPrice = price => _ui.price.getDomElement().innerHTML = (_price = price);
		this.setChange = change => _ui.change.getDomElement().innerHTML = (_change = change);
		this.setCompany = company => _ui.company.getDomElement().innerHTML = (_company = company);
		this.update = (onComplete) => {
			let data = 'company=' + this.getCompany();
			let onSuccess = request => {
				let data = JSON.parse(request.responseText);
				data.forEach(company => {
					this.setPrice(parseFloat(company.Price));
					this.setChange(parseFloat(company.Change));
				});
				onComplete();
			};
			ajaxRequest('POST', 'fetchData.php', true, data, onSuccess);
		}
		
		// Build the UI
		_createUI();	 
	};
	
	// Private function to construct the DOM elements needed for the the Widget.
	var _createUI = function() {
		// The assignments were previously in the Object literal but have been moved to the createUI function as per video.
		
		// The container the UI is contained within.
		_ui.container = container_element;
		// The dropdown menu providing a list of available Companies.
		_ui.companies = new Dropdown(
			'Companies', 
			(dropdown, selection) => {
				let shareLine = new ShareLine(selection.getName(), 0, 0);
				shareLine.onclick(() => {
					dropdown.addElement(selection.getName(), selection.getData());
					_ui.data.removeElement(shareLine);
				});
				shareLine.update(() => _ui.data.refresh());		
				_ui.data.addElement(shareLine);
				dropdown.removeElement(selection);
			}, 
			(a, b) => a.getData().localeCompare(b.getData())
		);
		// The update button to request updates on data that is currently monitored.
		_ui.update = new Button(
			'Update', 
			() => _ui.data.forEach(shareLine => shareLine.update(() => _ui.data.refresh()))
		);
		// The dropdown menu containing the sort functions to be applied to the data.
		_ui.sort = new Dropdown(
			'Sort By', 
			(dropdown, selection) => {
				let onclick = selection.getData();			
				if (onclick === _ui.data.getSort()) {
					_ui.data.setSort((a, b) => -onclick(a, b));
					dropdown.setName(`Sort By [${selection.getName()}] desc`);
				} else {
					_ui.data.setSort(onclick);
					dropdown.setName(`Sort By [${selection.getName()}] asc`);
				}				
			},
			(a, b) => a.getName().localeCompare(b.getName())
		);
		// The dropdown menu containing the various options overall for the widget.
		_ui.options = new Dropdown(
			'Options',
			(dropdown, selection) => selection.getData()()
		);
		// The list of ShareLine data that is monitored to be displayed.
		_ui.data = new List('shareline');
		
		// Ensure the container object is empty.
		_ui.container.innerHTML = null;
		
		// Construct the list of companies that can be monitored using the Widget.
		ajaxRequest('GET', 'fetchCompanies.php', true, null, request => {
			try {
				let data = JSON.parse(request.responseText);
				data.forEach(company => _ui.companies.addElement(company.Name, company.Name));
			} catch (e) {
				console.log("Could not load companies from database");
			}
		}); 		

		// Construct the list of sorting mechanisms that can be used with the Widget.		
		_ui.sort.addElement('Price', (a, b) => (a.getPrice() - b.getPrice()));
		_ui.sort.addElement('Change', (a, b) => (a.getChange() - b.getChange()));
		_ui.sort.addElement('Company', (a, b) => (a.getCompany().localeCompare(b.getCompany())));
		_ui.sort.getElement(1).click();

		// Construct the list of options that can be used with the Widget.
		_ui.options.addElement('Reset Widget', () => _createUI());
		_ui.options.addElement('Remove Widget', () => _ui.container.remove());

		// Local variables combining UI elements into containers.
		let _share_data = new Container('share-data', _ui.data);
		let _share_header = new Container('share-header', _ui.companies, _ui.update, _ui.sort, _ui.options);

		// Add the UI elements to the base container.
		_ui.container.appendChild(_share_header.getDomElement());
		_ui.container.appendChild(_share_data.getDomElement());
		_ui.container.className = 'share-widget';
	};
	
	// Private method to intialise the widget's UI on start up
	var _initialise = function(container_element) {
		_createUI(container_element);
	};
	
	// Initialise the Widget
	_initialise(container_element);
}

/**
 * ES Class for a DOMElement Object.
 *
 *  The DOMElement Object is, at it's core, a wrapper around a single HTMLElement
 *   that can be obtained by invoking the getDomElement function.
 *
 *  To retain the structure outlined in the assignment, some constructor functions were
 *   not converted to ES Classes that extend the core DOMElement class. Instead, it is assumed
 *   that any Object with the function getDomElement defined is in essence an extension of the
 *   DOMElement Object, and thus can be used to construct more complex models. The prefered
 *   approach would have been to have the ShareWidget and ShareLine Objects extend the DOMElement
 *   base Object.
 *
 */
class DomElement {
	/**
	 * The DOMElement Constructor.
	 *
	 * @param {String} baseElement : The name of the tag that is the base of the DOMElement Object.
	 * @param {String} className : The class name to assign tot he base DOMElement.
	 * @param {String} objectName : The name to be associated with the Object. While effectively
	 *  redundant in this Object implementation, Objects that inherit utilize this for different 
	 *  purposes.
	 *
	 */
	constructor(baseElement, className, objectName) {
		var _dom_element = document.createElement(baseElement);
		var _name = objectName;
		_dom_element.value = _name;
		_dom_element.className = className;
		
		this.getName = () => _name;
		this.getDomElement = () => _dom_element;
		this.setName = name => this.getDomElement().value = (_name = name);
	};

	/**
	 * Appends children to the base DOMElement by recursively finding the base DOMElement of the
	 *  Object. The method uses variable arguments to append many children in the order they are
	 *  listed as parameters in the function call. If there is no base DOMElement, the function
	 *  assumes it can be displayed as plain text (innerHTML).
	 *
	 * @param {varargs Object} elements : The elements to be appended to the base DOMElement.
	 *
	 */
	appendChild(...elements) {
		for (let element of elements) {
			if (element instanceof HTMLElement) {
				this.getDomElement().appendChild(element);
			} else if (typeof element.getDomElement === 'function') {
				this.appendChild(element.getDomElement());
			} else {
				this.getDomElement().innerHTML += element;
			}
		}
	};

	/**
	 * Appends children to the base DOMElement by recursively finding the base DOMElement of the
	 *  Object. The method uses an array of Objects to append many children in the order they are
	 *  listed in the array.
	 *
	 * @param {Array} elements : The elements to be appended to the base DOMElement.
	 *
	 */
	appendChildren(elements) {
		elements.forEach(element => this.appendChild(element));
	};

	/**
	 * Removes children from the base DOMElement by recursively finding the base DOMElement of
	 *  the Object. The method uses variable arguments to remove many children in the order they
	 *  are listed as parameters in the function call.
	 *
	 * @param {varargs Object} elements : The elements to be removed from the base DOMElement.
	 *
	 */
	removeChild(...elements) {
		for (let element of elements) {
			if (element instanceof HTMLElement) {
				this.getDomElement().removeChild(element);
			} else if (typeof element.getDomElement === 'function') {
				this.removeChild(element.getDomElement());
			}
		}
	}

	/**
	 * Sets the onclick property of the base DOMElement.
	 *
	 * @param {Function} event : The event to be triggered when the base DOMElement is clicked.
	 */
	setOnClick(event) {
		this.getDomElement().onclick = event;
	};

	/**
	 * Clicks the DomElement.
	 *
	 */
	click() {
		this.getDomElement().onclick();
	}

	/**
	 * Gets the style
	 *
	 */
	getStyle() {
		return this.getDomElement().style;
	}
}

/**
 * ES Class for a Button Object.
 *
 *  The Button Object is an extension of the DomElement Object. The Object is
 *   used to create a button using predefined CSS styling.
 *
 */
class Button extends DomElement {
	/**
	 * The Button Constructor.
	 *
	 * @param {String} name : The name to be associated with the Button. This value is displayed
	 *  as displayable text on the button.
	 * @param {String} onclick : The event to be triggered when the Button is clicked.
	 *
	 */
	constructor(name, onclick) {
		super('input', 'btn full-width', name);
		this.getDomElement().type = 'button';
		this.getDomElement().onclick = onclick;
	};
}

/**
 * ES Class for a List Object.
 *
 *  The List Object is an extension of the DomElement Object. The Object is used
 *   to construct a list of DOMElements and uses various styling options to control
 *   how the list contents are displayed.
 */
class List extends DomElement {
	/**
	 * The List Constructor.
	 *
	 * @param {String} className : The class name to assign to the base DOMElement.
	 * @param {varargs DOMElement} contents : The DOMElements to be added to the list.
	 *
	 */
	constructor(className, ...contents) {
		super('ul', className, null);
		
		var _data = new Array();
		var _comparator = undefined;
		
		for (content of contents) {
			_data.append(content);
		}

		this.refresh = function() {
			_data.sort(_comparator);
			this.getDomElement().innerHTML = null;
			_data.forEach(object => this.appendChild(object));
		}
		this.addElement = object => {
			_data.push(object);
			this.refresh();
		};
		this.removeElement = object => {
			this.removeChild(object);			
			_data.remove(object);			
		};
		this.empty = () => {
			_data = new Array();
			this.refresh();
		};
		this.setSort = sort => {
			_comparator = sort;
			this.refresh();
		};
		this.getData = () => _data;
		this.getSort = () => _comparator;
		this.getElement = index => _data[index];

		this.refresh();
	};

	/**
	 * Iterates over the contents of the list, invoking a given function with each
	 *  item as a parameter.
	 *
	 * @param {Function} action : The action to be performed on each item in the list.
	 *
	 */
	forEach(action) {
		this.getData().forEach(action);
	};
}

/**
 * ES Class for a Dropdown Object.
 *
 *  The Dropdown Object is an extension of the DomElement Object. The Object is used
 *   to construct a Dropdown menu by wrapping an instance of the Button and List Objects.
 *   In essence, the Dropdown Object displays a list when a Button is hovered.
 *
 */
class Dropdown extends DomElement {
	/**
	 * The Dropdown Constructor.
	 *
	 * @param {String} name : The name to be associated with the Dropdown. This value is displayed
	 *  as displayable text on the button wrapped in the Object.
	 * @param {Function} onSelect : Function to be invoked when an item is selected from the dropdown
	 *  menu. The function will be invoked with the selected item as a parameter.
	 * @param {Function} sort : Function to be used to sort the data within the list wrapped in the
	 *  Object.
	 *
	 */
	constructor (name, onSelect, sort) {
		super('div', 'dropdown', name);
		
		var _button = new Button(name);
		var _list = new List('content');
		var _self = this;

		_list.setSort(sort);			
		this.appendChild(_button, _list);

		this.setName = name => _button.setName(name);		
		this.getElement = index => _list.getElement(index);
		this.removeElement = object => _list.removeElement(object);
		this.addElement = (name, data) => {
			let object = new DropdownOption(name, data);
			object.setOnClick(() => onSelect(_self, object));
			_list.addElement(object);
		};
		this.empty = () => _list.empty();
	};
}

/**
 * ES Class for a DropdownOption Object.
 *
 *  The DropdownOption Object is an extension of the DomElement Object. The Object is used
 *   to construct display the various items selectable within the Dropdown Object.
 *
 */
class DropdownOption extends DomElement {
	/**
	 * The Dropdown Constructor.
	 *
	 * @param {String} name : The name to be associated with the DropdownOption Object. This is
	 *  displayed as plain text within the base DOMElement (innerHTML).
	 * @param {Object} data : The data to be held within the DropdownOption Object.
	 *
	 */
	constructor (name, data) {
		super('a', '', name);
		var _data = data;	
		
		this.getData = () => _data;
		this.setData = data => _data = data;
		this.getDomElement().innerHTML = name;
	};
}

/**
 * ES Class for a Container Object.
 *
 *  The Container Object is an extension of the DomElement Object. The Object is used
 *   to wrap around a set of elements. The primary purpose is to combine DOMElements into 
 *   a single Object and enforce certain CSS formatting.
 *
 */
class Container extends DomElement {
	/**
	 * The List Constructor.
	 *
	 * @param {String} className : The class name to assign to the base DOMElement.
	 * @param {varargs DOMElement} contents : The DOMElements to be added to the container.
	 *
	 */
	constructor (className, ...contents) {
		super('div', className, 'container');
		this.appendChildren(contents);
	};
}