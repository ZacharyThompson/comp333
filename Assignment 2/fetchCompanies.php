<?php
/**
 * This file is used to fetch the companies that are stored in the database.
 *
 * @author Zachary Thompson (1287280)
 */

// Attempt a connection to the database.
require_once("dbconnect.php");

// Construct a query that selects the data relating to the given company.
$query = "SELECT Name FROM shareprices";

// Execute the query and respond the data in a friendly format.
$result = $con->query($query);
echo json_encode($result->fetchAll(PDO::FETCH_ASSOC));
