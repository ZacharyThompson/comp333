<?php
/**
 * This file is used to process a login request from the client.
 *
 * @author Zachary Thompson (1287280)
 */
include("common.php");

// Attempt a connection to the database.
require_once("dbconnect.php");	


function sendError($error) {
	notAcceptableError($error);
}

// Construct a query to determine whether the given username is taken.
$query = "SELECT * FROM users WHERE username='" . $_POST['username'] . "';";
$result = $con->query($query);
	
// Check that the number of arguments given is sufficient.
if (count($_POST) != 5) {
	sendError("Invaild arguments");
	return;
}

// Validate Firstname
if (!isset($_POST['firstname']) || strlen($_POST['firstname']) == 0) {
	sendError("Invalid Firstname");
	return;
}
	
// Validate Lastname
if (!isset($_POST['lastname']) || strlen($_POST['lastname']) == 0) {
	sendError("Invalid Lastname");
	return;
}
	
// Validate Email
if (!isset($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
	sendError("Invalid Email");
	return;
}	

// Validate Password
if (!isset($_POST['password']) ||
	strlen($_POST['password']) < 8 ||
       	!ctype_alnum($_POST['password']) ||
	!preg_match("/[0-9]/", $_POST['password']) ||
	!preg_match("/[a-zA-z]/", $_POST['password'])
) {
	sendError("Invalid Password");
	return;
}

// Check if the username has been taken	
if ($result->rowCount() > 0) {
	sendError("User Already Exists");
	return;
}

// Add the user using the given data.
$rows = implode(",", array_keys($_POST));
$values = implode("','", $_POST);	
$query = "INSERT INTO users ($rows) 
	VALUES ('$values');";

// Attempt the insertion of the database.
$result = $con->query($query);

// If the insertion failed for any reason, throw an error.
if (!result) {
	throwError("Registration Failed");
}

	
