// ToDo
var now = new Date();
now.setHours(now.getHours() - 24);
var date = Math.round(now.getTime() / 1000);


/**
 * Startup Sequence
 */
window.addOnload(function() {
	resetMessageCache();
	fetchGlobal();
	refreshUsers();
});

/**
 * Refreshes the user list which describes known user activity.
 *
 */
var fetchUserPoller;
function refreshUsers() {
	"use strict";
	let url = "refreshUsers.php";
	ajaxRequest("GET", url, true, "", request => {
		document.getElementById("users").innerHTML = request.responseText;
	});

	clearInterval(fetchUserPoller);
	fetchUserPoller = setInterval(refreshUsers, 1000);
}

/**
 * Triggers a registration request based upon the register-block form.
 *
 * @return {boolean} False. Prevents a page refresh upon form submission.
 */
function registerUser() {
	"use strict";
	let data = serializeForm(document.forms['register-block']);
	
	let onSuccess = request => {
		showLogin();
	};

	let onFailure = request => {
		if (request.status === 406) {
			alert(JSON.parse(request.response)['message']);
		} else {
			alert("Something went wrong while attempting registration");
	};

	ajaxRequest("POST", "registerUser.php", true, data, onSuccess, onFailure);
	return false;
}

/**
 * Triggers a login request based upon the login-block form.
 *
 * @return {boolean} False. Prevents a page refresh upon form submission.
 */
function loginUser() {
	"use strict";
	let data = serializeForm(document.forms['login-block']);

	let onSuccess = request => {
		showContent();
		fetchGlobal();
		refreshUsers();	
	};

	let onFailure =  request => {
		if (request.status === 403) {
			alert(JSON.parse(request.response)['message']);
		} else {
			alert("Something went wrong attempting to login");
		}
	};

	ajaxRequest("POST", "loginUser.php", true, data, onSuccess, onFailure);
	return false;
}

/**
 * The function used to logout of the dynamic website.
 *
 */
function logout() {
	"use strict";
	ajaxRequest("GET", "logout.php", true, null, refreshUsers);	
	showLogin();
}

/**
 * The function used to hide primary content, the messages, and display
 * the login page.
 *
 */
function showLogin() {
	"use strict";
	resetMessageCache();
	fetchParams = null;
	document.getElementById("login-form").style.display = "block";
	document.getElementById("primary-content").style.display = "none";
}

/**
 * The function used to show the primary content, the messages, and hide
 * the login page.
 *
 */
function showContent() {
	"use strict";
	resetMessageCache();
	document.getElementById("login-form").style.display = "none";
	document.getElementById("primary-content").style.display = "block";
	toggleNavbar(document.getElementsByClassName("nav-link")[0]);
}

/**
 * The function used to poll the server for message updates. The paramets are
 * used to define what sort of messages the server should be fetching.
 *
 */
var fetchParams = null;
var fetchPoller;
function fetch() {
	"use strict";
	if (fetchParams != null) {
		fetchMessages(fetchParams[0],fetchParams[1],fetchParams[2]);
	}
	clearInterval(fetchPoller);
	fetchPoller = setInterval(fetch, 1000);
}

/**
 * The function used to request messages from the server using the recipient
 * and sender as filters. The type is used for formatting purposes.
 *
 *
 * @param {String} Type. The type of message that is being request. Dictates
 *    how the element is displayed using HTML/CSS.
 * @param {String} From. The filter that defines who the messages should be from.
 *    Allows for special String literals such as "ALL", "NONE" and "ME".
 * @param {String} To. The filter that defines who the messages should be from.
 *    Allows for special String literals such as "ALL", "NONE" and "ME".
 */
function fetchMessages(type, from, to) {
	"use strict";
	let data = "from=" + from + "&to=" + to + "&date=" + date + "&id=" + messageCache;
	
	let onSuccess = request => {
		let json = JSON.parse(request.responseText);
		let messages = document.getElementById("messages");
		json.forEach(element => {
			let message = formatMessage(type, element);
            		messages.appendChild(message);			
			messages.scrollTop += message.offsetHeight;
			messageCache = element['id'];
		});
	};

	let onError = request => {
        	if (request.status === 403) {
            	showLogin();
        	} else {
				alert("Something went wrong attempting to fetch messages");
			}
    	};

	ajaxRequest("POST", "fetchMessages.php", true, data, onSuccess, onError);
}

/**
 * Function used to reset the message cache. This is done by removing all
 * previously displayed messages and resetting the message cache integer
 * so that the next fetch requests all messages.
 *
 */
var messageCache;
function resetMessageCache() {
	"use strict";
	document.getElementById("messages").innerHTML = null;
	messageCache = -1;
}

/**
 * Set fetch mode to global and initiate another fetch.
 *
 */
function fetchGlobal() {
	"use strict";
	resetMessageCache();
	fetchParams = ['global', 'ALL', 'NONE'];
	fetch();
}

/**
 * Set fetch mode to inbox and initiate another fetch.
 *
 */
function fetchInbox() {
	"use strict";
	resetMessageCache();
	fetchParams = ['inbox', 'ALL', 'ME'];
	fetch();
}

/**
 * Set fetch mode to outbox and intitiate another fetch.
 *
 */
function fetchOutbox() {
	"use strict";
	resetMessageCache();
	fetchParams = ['outbox', 'ME', 'ALL'];
	fetch();
}

/**
 * The function used to initiate a send message request to the server.
 * Each message is expected to have a sender, recipient and message content.
 * It is assumed that neglecting to fill this fields in intentional and the
 * server should respond accomodate any reasonable requests, however, will
 * reject requests which are invalid or where permissions are insufficient.
 *
 */
function sendMessage(from, to, message, onSent) {
	"use strict";
	let data = "from=" + from + "&to=" + to + "&message=" + message;

	let onSuccess = request => {
		onSent();
		fetch();
	};

	let onFailure = request => {
		if (request.status === 403) {
			alert(JSON.parse(request.response)['message']);
		} else {
			alert("Something went wrong attempting to send the message");
		}
	};

	ajaxRequest("POST", "sendMessage.php", true, data, onSuccess, onFailure);
}

/**
 * The function used to process a message. This is intended as input vaildation
 * for the specific message input case. This does not generify (ToDo).
 *
 */
function processMessage() {
	"use strict";
	let check = document.getElementById("recipient-check");
	let to = check.checked ? "ALL" : document.getElementById("recipient-input").value;
	let message = document.getElementById("message-input").value;

	if (to.length === 0) {
		alert("No user entered");
	} else if (message.length < 6) {
		alert("Message too short");
	} else {
		sendMessage("ME", to, message, function() {
			document.getElementById("message-input").value = "";
		});
	}
}

/**
 * Formats a message from a JSON associative array.
 *
 * @param {String} Type. The message type used to define how the message
 *    should be formatted.
 * @param {Object} Data. The JSON assoaciative array that contains the message
 *    data to be transformed into a displayable format.
 */
function formatMessage(type, data) {
	"use strict";
	let message = document.createElement("li");
	message.classList.add("message", type);

	let toDiv = document.createElement("div");
	toDiv.classList.add("header", "to");
	toDiv.innerHTML = data.to_user;
	message.append(toDiv);

	let fromDiv = document.createElement("div");
	fromDiv.classList.add("header", "from");
	fromDiv.innerHTML = data.from_user;
	message.append(fromDiv);

	let dateDiv = document.createElement("div");
	dateDiv.classList.add("date");
	dateDiv.innerHTML = " (" + data.date + ") :";
	message.append(dateDiv);

	let messageDiv = document.createElement("div");
	messageDiv.classList.add("body");
	messageDiv.innerHTML = data.message;
	message.append(messageDiv);

	return message;
}



