<?php
/**
 * This file is used to process a send message request from the client.
 *
 * @author Zachary Thompson (1287280)
 */
include("common.php");

// Start a session and attempt a connection to the database.
session_start();
require_once("dbconnect.php");

// Update the activity of the current user.
updateActivity($con);

// Function used to filter keywords to an appropriate SQL condition.
function filter($value) {
	if ($value == null) return "NULL";
	if ($value == "NONE") return "NULL";
	if ($value == "ME") return "'".$_SESSION['username']."'";
	if ($value == "ALL") return "NULL";
	return "'".$value."'";
}

// Format the values for the SQL insert query
$to = filter($_POST["to"]);
$from = filter($_POST["from"]);
$message = "'".$_POST["message"]."'";

// If the recipient doesn't exist, send a bad request to the client.
if (!userExists($con, $to) && $to != "NULL") {
	badRequestError("Invalid Recipient");
	return;
}

// If the sender doesn't exist, send a bad request to the client.
if (!userExists($con, $from) && $from != "NULL") {
	badRequestError("Invalid Sender");
	return;
}    

$query = "INSERT INTO messages (from_user, to_user, message) VALUES ($from, $to, $message);";

// Attempt to send the message
$result = $con->query($query);
if ($result) {
	echo "Message successfully sent";
} else {
	echo "An error occurred";
}
