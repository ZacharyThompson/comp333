<?php
/**
 * This file is used to process a login request from the client.
 *
 * @author Zachary Thompson (1287280)
 */
include("common.php");

// Start a session and attempt a connection to the database.
session_start();
require_once("dbconnect.php");

// Update the activity of the current user.
updateActivity($con);

// Construct a query that checks whether the given user exists within the database.
$query = "SELECT * FROM users WHERE
	username='{$_POST['username']}' AND
	password='{$_POST['password']}';";

// Invoke the query on the database
$result = $con->query($query);

// Check if there were any results form the database
if ($result->rowCount() == 1) {
	// Login was successful	
	$_SESSION['username'] = $_POST['username'];
	updateActivity($con);
} else {
	// Login was unscucessful
	forbiddenError();
	$_SESSION['username'] = "";
}
