<?php
/**
 * This file is used to process a fetch message request by the client.
 *
 * @author Zachary Thompson (1287280)
 */
include("common.php");

// Start a session and attempt a connection to the database.
session_start();
require_once("dbconnect.php");

// Update user activity
updateActivity($con);

// Function used to filter keywords to an appropriate SQL condition.
function filter($value) {
    if ($value == null) return "IS NULL";
    if ($value == "ALL") return "IS NOT NULL";
    if ($value == "NONE") return "IS NULL";
    if ($value == "ME") return "= '" . $_SESSION['username'] . "'";
    return "= '" . $value . "'";
}

// Function used to determine whether the user possesses the relevent permissions.
function checkPermission($to, $from) {
    if (!isset($_SESSION['username']) || $_SESSION['username'] == "")
        return false;
    if  ($to == null || $to == "ALL" || $to == "NONE" || $to == "ME" || $from == "ME")
        return true;
    return false;
}

// Check if the user has the required permissions to read the message.
if (!checkPermission($_POST['to'], $_POST['from'])) {
    forbiddenError();
    return;
}

// Format the values for the SQL insert query.
$to = filter($_POST['to']);
$from = filter($_POST['from']);
$date = date("Y-m-d H:i:s", $_POST['date']);
$id = $_POST['id'];

// Construct the query to fetch all request messages.
$query = "SELECT * FROM messages WHERE
	to_user $to 
	AND from_user $from 
	AND date > '$date' 
	AND id > $id
	ORDER BY date;";

// Execute the query and respond with all values.
$result = $con->query($query);
echo json_encode($result->fetchAll(PDO::FETCH_ASSOC));
