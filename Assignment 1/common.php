<?php
/**
 * This file is a collection of common methods that are shared amongst the intent specific php scripts.
 *
 * @author Zachary Thompson (1287280)
 */

/**
 * Updates the activity field for the user associated with the current session.
 *  This is used primarily to keep record of which users are currently logged in.
 *
 * @param $con The PDO database connection to connect to.
 */
function updateActivity($con) {
	if (isset($_SESSION['username'])) {
		$now = date("Y-m-d H:i:s", time());
		$query = "UPDATE users SET last_activity = '$now' WHERE username = '{$_SESSION['username']}';";
		$result = $con->query($query);
	}
}

/**
 * Issues a forbidden HTTP error.
 */
function forbiddenError() {
	header('HTTP/1.1 403 Forbidden');
	header('Content-Type: application/json; charset=UTF-8');
	die(json_encode(array('message' => 'ERROR', 'code' => 2)));
}

/**
 * Issues a bad request HTTP error.
 */
function badRequestError($msg) {
	header('HTTP/1.1 400 Bad Request');
	header('Content-Type: application/json; charset=UTF-8');
	die(json_encode(array('message' => $msg, 'code' => 2)));
}

function notAcceptableError($msg) {
	header('HTTP/1.1 406 Not Acceptable');
	header('Content-Type: application/json; charset=UTF-8');
	die(json_encode(array('message' => $msg, 'code' => 2)));
}

/**
 * Checks whether a user exists within the database.
 *
 * @param $con The PDO database connection to connect to.
 * @param $user The username to check for existence in the database.
 */

 function userExists($con, $user) {
	$query = "SELECT * FROM users WHERE username = $user;";
	$result = $con->query($query);
	return ($result->rowCount());
}

