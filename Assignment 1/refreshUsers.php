<?php
/**
 * This file is used to process a refresh user request from the client.
 *
 * @author Zachary Thompson (1287280)
 */
include("common.php");

// Start a session and attempt a connection to the database.
session_start();
require_once("dbconnect.php");	

// Update the activity of the current user.
updateActivity($con);

// Select all users from the database and the time of there last activity
$query = "SELECT username, last_activity FROM users ORDER BY last_activity desc, username";
$result = $con->query($query);

while($row = $result->fetch()) {
	// If the user has been active recently display the user as online.
	$time = time() - strtotime($row["last_activity"]);
	$online = $time < (60 * 60) ? "online" : "offline";
	// Construct an HTML list element to display the individual user.
	$username = (isset($_SESSION['username']) && $_SESSION['username'] == $row['username']) ? "user" : "";
	$user = "<li class='user'>
		{$row['username']}
		<span class='status $online $username'></span>
		</li>";
	echo $user;
}
