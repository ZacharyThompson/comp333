<?php
/**
 * This file is used to process a logout request from the client.
 *
 * @author Zachary Thompson (1287280)
 */

// Start a session and attempt a connection to the database.
session_start();
require_once("dbconnect.php");

// If there is a user logged in, log them out.
if (isset($_SESSION['username'])) {
	$now = 0;
	$now = date("Y-m-d H:i:s", $now);
	$query = "UPDATE users SET last_activity = '{$now}' WHERE username = '{$_SESSION['username']}';";
	$result = $con->query($query);
}

// Destroy the session
session_destroy();

