<?php
	if (isset($_POST)) {
		// Get the parameters form the post data
		$key = "AIzaSyBJxiQhJwLksEO36m7q0Ree2DBoH6MSxTU";
		$keyword = $_POST['keyword'];
		$lng = $_POST['lng'];
		$lat = $_POST['lat'];
		$radius = $_POST['radius'];
		// Form the request URL
		$request = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location={$lat},{$lng}&radius={$radius}&keyword={$keyword}&key={$key}";
		// Initiate a connection	
		$connection = curl_init($request);
		// Set parameters
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST,  2);
		// Execute request	
		$response = curl_exec($connection);
		// Close connection	
		curl_close($connection);
		// Return response	
		echo $response;
	}
	
