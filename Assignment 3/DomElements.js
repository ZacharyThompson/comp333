/**
 * ES Class for a DOMElement Object.
 *
 *  The DOMElement Object is, at it's core, a wrapper around a single HTMLElement
 *   that can be obtained by invoking the getDomElement function.
 *
 *  To retain the structure outlined in the assignment, some constructor functions were
 *   not converted to ES Classes that extend the core DOMElement class. Instead, it is assumed
 *   that any Object with the function getDomElement defined is in essence an extension of the
 *   DOMElement Object, and thus can be used to construct more complex models. The prefered
 *   approach would have been to have the ShareWidget and ShareLine Objects extend the DOMElement
 *   base Object.
 *
 */
class DomElement {
	/**
	 * The DOMElement Constructor.
	 *
	 * @param {String} baseElement : The name of the tag that is the base of the DOMElement Object.
	 * @param {String} className : The class name to assign tot he base DOMElement.
	 * @param {String} objectName : The name to be associated with the Object. While effectively
	 *  redundant in this Object implementation, Objects that inherit utilize this for different 
	 *  purposes.
	 *
	 */
	constructor(baseElement, className, objectName) {
		var _dom_element = document.createElement(baseElement);
		var _name = objectName;
		_dom_element.value = _name;
		_dom_element.className = className;
		
		this.getName = () => _name;
		this.getDomElement = () => _dom_element;
		this.setName = name => this.getDomElement().value = (_name = name);
	};

	/**
	 * Appends children to the base DOMElement by recursively finding the base DOMElement of the
	 *  Object. The method uses variable arguments to append many children in the order they are
	 *  listed as parameters in the function call. If there is no base DOMElement, the function
	 *  assumes it can be displayed as plain text (innerHTML).
	 *
	 * @param {varargs Object} elements : The elements to be appended to the base DOMElement.
	 *
	 */
	appendChild(...elements) {
		for (let element of elements) {
			if (element instanceof HTMLElement) {
				this.getDomElement().appendChild(element);
			} else if (typeof element.getDomElement === 'function') {
				this.appendChild(element.getDomElement());
			} else {
				this.getDomElement().innerHTML += element;
			}
		}
	};

	/**
	 * Appends children to the base DOMElement by recursively finding the base DOMElement of the
	 *  Object. The method uses an array of Objects to append many children in the order they are
	 *  listed in the array.
	 *
	 * @param {Array} elements : The elements to be appended to the base DOMElement.
	 *
	 */
	appendChildren(elements) {
		elements.forEach(element => this.appendChild(element));
	};

	/**
	 * Removes children from the base DOMElement by recursively finding the base DOMElement of
	 *  the Object. The method uses variable arguments to remove many children in the order they
	 *  are listed as parameters in the function call.
	 *
	 * @param {varargs Object} elements : The elements to be removed from the base DOMElement.
	 *
	 */
	removeChild(...elements) {
		for (let element of elements) {
			if (element instanceof HTMLElement) {
				this.getDomElement().removeChild(element);
			} else if (typeof element.getDomElement === 'function') {
				this.removeChild(element.getDomElement());
			}
		}
	}

	/**
	 * Sets the onclick property of the base DOMElement.
	 *
	 * @param {Function} event : The event to be triggered when the base DOMElement is clicked.
	 */
	setOnClick(event) {
		this.getDomElement().onclick = event;
	};

	/**
	 * Clicks the DomElement.
	 *
	 */
	click() {
		this.getDomElement().onclick();
	}

	/**
	 * Gets the style
	 *
	 */
	getStyle() {
		return this.getDomElement().style;
	}
}

/**
 * ES Class for a Button Object.
 *
 *  The Button Object is an extension of the DomElement Object. The Object is
 *   used to create a button using predefined CSS styling.
 *
 */
class Button extends DomElement {
	/**
	 * The Button Constructor.
	 *
	 * @param {String} name : The name to be associated with the Button. This value is displayed
	 *  as displayable text on the button.
	 * @param {String} onclick : The event to be triggered when the Button is clicked.
	 *
	 */
	constructor(name, onclick) {
		super('input', 'btn full-width', name);
		this.getDomElement().type = 'button';
		this.getDomElement().onclick = onclick;
	};
}

/**
 * ES Class for a List Object.
 *
 *  The List Object is an extension of the DomElement Object. The Object is used
 *   to construct a list of DOMElements and uses various styling options to control
 *   how the list contents are displayed.
 */
class List extends DomElement {
	/**
	 * The List Constructor.
	 *
	 * @param {String} className : The class name to assign to the base DOMElement.
	 * @param {varargs DOMElement} contents : The DOMElements to be added to the list.
	 *
	 */
	constructor(className, ...contents) {
		super('ul', className, null);
		
		var _data = new Array();
		var _comparator = undefined;
		var _selected;
		
		for (content of contents) {
			_data.append(content);
		}

		this.refresh = function() {
			_data.sort(_comparator);
			this.getDomElement().innerHTML = null;
			_data.forEach(object => this.appendChild(object));
		}
		this.addElement = object => {
			_data.push(object);
			this.refresh();
		};
		this.removeElement = object => {
			this.removeChild(object);			
			_data.removeObject(object);			
		};
		this.empty = () => {
			_data = new Array();
			this.refresh();
		};
		this.setSort = sort => {
			_comparator = sort;
			this.refresh();
		};
		this.getData = () => _data;
		this.getSort = () => _comparator;
		this.getElement = index => _data[index];
		this.getSelected = () => _selected;
		this.setSelected = (selected) => {
			_selected = selected;
		};
		this.refresh();
	};

	/**
	 * Iterates over the contents of the list, invoking a given function with each
	 *  item as a parameter.
	 *
	 * @param {Function} action : The action to be performed on each item in the list.
	 *
	 */
	forEach(action) {
		this.getData().forEach(action);
	};
}

/**
 * ES Class for a Dropdown Object.
 *
 *  The Dropdown Object is an extension of the DomElement Object. The Object is used
 *   to construct a Dropdown menu by wrapping an instance of the Button and List Objects.
 *   In essence, the Dropdown Object displays a list when a Button is hovered.
 *
 */
class Dropdown extends DomElement {
	/**
	 * The Dropdown Constructor.
	 *
	 * @param {String} name : The name to be associated with the Dropdown. This value is displayed
	 *  as displayable text on the button wrapped in the Object.
	 * @param {Function} onSelect : Function to be invoked when an item is selected from the dropdown
	 *  menu. The function will be invoked with the selected item as a parameter.
	 * @param {Function} sort : Function to be used to sort the data within the list wrapped in the
	 *  Object.
	 *
	 */
	constructor (name, onSelect, sort) {
		super('div', 'dropdown', name);
		
		var _button = new Button(name);
		var _list = new List('content');
		var _self = this;

		_list.setSort(sort);			
		this.appendChild(_button, _list);

		this.setName = name => _button.setName(name);		
		this.getElement = index => _list.getElement(index);
		this.removeElement = object => _list.removeElement(object);
		this.addElement = (name, data) => {
			let object = new DropdownOption(name, data);
			object.setOnClick(() => {
				_list.setSelected(object);
				onSelect(_self, object);
			});
			_list.setSelected(object);
			_list.addElement(object);
		};
		this.getSelected = () => _list.getSelected();
		
		this.empty = () => _list.empty();
	};
}

/**
 * ES Class for a DropdownOption Object.
 *
 *  The DropdownOption Object is an extension of the DomElement Object. The Object is used
 *   to construct display the various items selectable within the Dropdown Object.
 *
 */
class DropdownOption extends DomElement {
	/**
	 * The Dropdown Constructor.
	 *
	 * @param {String} name : The name to be associated with the DropdownOption Object. This is
	 *  displayed as plain text within the base DOMElement (innerHTML).
	 * @param {Object} data : The data to be held within the DropdownOption Object.
	 *
	 */
	constructor (name, data) {
		super('a', '', name);
		var _data = data;	
		
		this.getData = () => _data;
		this.setData = data => _data = data;
		this.getDomElement().innerHTML = name;
	};

	equals(object) {
		let data = (typeof object.getName === 'function') ? object.getName() : object;
		return object === this.getName();
	}
}

/**
 * ES Class for a Container Object.
 *
 *  The Container Object is an extension of the DomElement Object. The Object is used
 *   to wrap around a set of elements. The primary purpose is to combine DOMElements into 
 *   a single Object and enforce certain CSS formatting.
 *
 */
class Container extends DomElement {
	/**
	 * The List Constructor.
	 *
	 * @param {String} className : The class name to assign to the base DOMElement.
	 * @param {varargs DOMElement} contents : The DOMElements to be added to the container.
	 *
	 */
	constructor (className, ...contents) {
		super('div', className, 'container');
		this.appendChildren(contents);
	};
}
