<?php
	if (isset($_POST)) {
		// Get the lng/lat values from the POST data
		$lng = $_POST['lng'];
		$lat = $_POST['lat'];
		// Form the request URL
		$request = "https://api.sunrise-sunset.org/json?lat={$lat}&lng={$lng}&date=today&formatted=0";
		// Initiate a connection
		$connection = curl_init($request);
		// Set parameters
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST,  2);
		// Execute request	
		$response = curl_exec($connection);
		// Close connection	
		curl_close($connection);
		// Return response	
		echo $response;
	}
