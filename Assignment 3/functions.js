var geocoder;
var map;
var visited;
var interest;
var markers = [];
var infowindow;

/**
 * Initiates the website by instantiating the dropdown controls and the map.
 *  The map created is loaded to the town closest associated with the users
 *  ip address - This is done by searching for the town in New Zealand and
 *  adding the result to the visited dropdown menu such that there exists
 *  a town to begin with such that meaningful data can be loaded initially. 
 *
 */
function initMap() {
	// Initiate dropdown controls
	initDropdown();
	
	// Initiate geocoder and display full map
	geocoder = new google.maps.Geocoder();
	map = new google.maps.Map(document.getElementById('map'), {
		zoom : 1,
		center : {lat: 0, lng: 0}
	});
	// Initiate information window to be used for place markers
	infowindow = new google.maps.InfoWindow();
	
	// Finds the address associated with the users ip address and
	// locates that town.
	ajaxRequest('GET', 'ip.php', true, null, request => {
		let data = JSON.parse(request.responseText);
		queryTown({'address' : data.city, 'region' : 'NZ'}, true);
		document.getElementById('town-form-region').value = data.country;
		document.getElementById('town-form-address').value = data.city;
	});
}

/**
 * Initiates the dropdown controls, specifically the visited and interest
 *  dropdowns that contain data relating to the visited towns and the
 *  interested places, respectively.
 * 
 */
function initDropdown() {
	// Initiates and adds the dropdown menu for the visited towns.
	visited = new Dropdown("Visited", (dropdown, selection) => {
		moveMap(selection.getData());
	});
	document.getElementById("visited").appendChild(visited.getDomElement());

	// Initiates and adds the dropdwon menu for the interested places.
	interest = new Dropdown("Interest", (dropdown, selection) => {
		// Get the place associated with the selected item
		let place = selection.getData();
		// Create a marker on the map for the place
		var marker = new google.maps.Marker({
			map: map,
			place: {
				placeId: place.place_id,
				location: place.geometry.location
			}
		});
		// And an onClick listener to the marker to display place information
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.setContent(
				'<div><strong>' + place.name + '</strong><br>' +
				'Place ID: ' + place.place_id + '<br>' +
				place.vicinity + '</div>'
			);
			infowindow.open(map, this);
		});
		// Save the marker
		markers.push(marker);
	});
	document.getElementById("interest").appendChild(interest.getDomElement());
}

/**
 * Changes the focus of the map to a given a town, displaying the relevant
 *  town information which has been cached within the town Object. 
 *
 * @param {Town} The Town object to move the map to.
 */
function moveMap(town) {
	// Load the primary town content container
	let content = document.getElementById("town-content");
	// Clear the contents of the town content container and add the town information
	content.innerHTML = null;
	content.appendChild(town.getDomElement());
	clearMarkers();	
	// Move the map to the bounds of the selected town
	map.fitBounds(town.getBounds());	
}

/**
 * Queries for a town using the Google geocoder and creates a Town object
 *  for the closest matching town.
 *
 * @param {Object} Object literal representing the town query to be performed
 * @param {Boolean} Whether the results should be cached
 */
function queryTown(query, cache) {
	geocoder.geocode(query, (results, status) => {
			if (status === 'OK') {
				let match = 0;
				
				// Ensure the match is a city
				for (match = 0; match < results.length; match++) {					
					if (results[match].types.contains('locality') || 
						results[match].types.contains('political')) {
						break;
					}
				}

				// If no town was found, prompt the user.
				if (match >= results.length || results[match].geometry.bounds == undefined) {
					alert('No location was found');
					return;
				}

				// Instantiate a town object form the geocoding results
				let town = new Town(results[0]);
				let data = `lat=${town.getLocation().lat()}&lng=${town.getLocation().lng()}`;				

				// Get the sunset/sunrise information for the 
				ajaxRequest('POST', 'sunset.php', true, data, request => {
					town.setSun(JSON.parse(request.responseText).results);
				});
			
				// Get the timezone of the given town
				ajaxRequest('POST', 'timezone.php', true, data, request => {
					town.setOffset(JSON.parse(request.responseText));
				});

				// Get the weather forecast of the given town
				ajaxRequest('POST', 'forecast.php', true, data, request => {
					town.setWeather(JSON.parse(request.responseText));
				});
				
				// Save the town if a cache request was made
				if (cache) {		
					// Remove any conflicting entries in the visited dropdown
					visited.removeElement(town.getAddress());
					// Add the given town to the dropdown menu
					visited.addElement(town.getAddress(), town);
				}

				// Move the map
				moveMap(town);
			}
		}
	);
}

/**
 * Finds a town using the information provided on the town form. This function is
 *  invoked by the town form, and as such returns false as to avoid a page reload
 *  to ensure dynamic behaviour of the website.
 *
 */
function findTown() {
	// Get the data from the town form
	let data = getValues(document.forms['town-form']);
	// Form a query request
	let query = {
		'address' : data.address + " " + data.region
	};
	// Query for the town
	queryTown(query, true);
	// Prevents page reload	
	return false;
}

/**
 * Searches for places using the information provided on the interest form. This
 *  function is invoked by the interest form, and as such returns false as to avoid
 *  a page reload to ensure dynamic behaviour of the website.
 *
 */
function searchPlaces() {
	// If there is no currently selected visited town, don't do anything
	if (visited.getSelected() == undefined)
		return;

	// Load the data used to form a place search request
	let data = "keyword=" + getValues(document.forms['interest-form'])['interest'];
	data += "&lat=" + map.getCenter().lat();
	data += "&lng=" + map.getCenter().lng();
	data += "&radius=" + getRadius(visited.getSelected().getData().getBounds());

	// Find the places associated with the formed data request
	ajaxRequest('POST', 'places.php', true, data, request => {
		// Clear the markers
		clearMarkers();	
		// Load the data
		let results = JSON.parse(request.responseText).results;
		// Add each result to the interest dropdown
		for (let i = 0; i < results.length; i++) {
			interest.addElement(results[i].name, results[i]);
		} 
	});

	// Prevents a page reload
	return false;
}

/**
 * Clears all the markers that have been added to the map and makes the Objects
 *  eligible for garbage collection.
 */
function clearMarkers() {
	// Empty the interest dropdown
	interest.empty();
	// Remove the markers form the map
	for (let i = 0; i < markers.length; i++) {
		markers[i].setMap(null);
	}
	// Allow garbage collection to do the rest
	markers = [];
}

class Town extends List {
	// Constructs a Town object
	constructor (result) {
		super('town');

		// Private variables
		var _address = result.formatted_address;
		var _bounds = result.geometry.bounds;
		var _location = result.geometry.location;
		var _sunset;
		var _sunrise;
		var _offset = 0;
		var _weather = new List('weather');

		// Getters
		this.getAddress = () => _address;
		this.getBounds = () => _bounds;
		this.getLocation = () => _location;
		this.getSunrise = () => _sunrise;
		this.getSunset = () => _sunset;
		this.getOffset = () => _offset;
		this.getWeather = () => _weather;

		// Setters
		this.setSun = (results) => {
			_sunrise = new Date(results.sunrise);
			_sunset = new Date(results.sunset);
			this.update();
		};
		this.setOffset = (offset) => {
			_offset = offset.dstOffset + offset.rawOffset;
			this.update();
		};
		this.setWeather = (data) => {
			for  (let i = 0; i < data.list.length; i++) {
				this.getWeather().addElement(new WeatherInfo(data.list[i]));
			}
		};
	};

	// Performs a visual update to the description DOMElement	
	update() {
		if (this.getSunset() == undefined || this.getSunrise() == undefined)
			return;
		let timeSunset = this.getSunset().getTime() + this.getOffset() * 1000 + this.getSunset().getTimezoneOffset() * 60 * 1000;
		let localSunset = new Date(timeSunset);
		let timeSunrise = this.getSunrise().getTime() + this.getOffset() * 1000 + this.getSunrise().getTimezoneOffset() * 60 * 1000;
		let localSunrise = new Date(timeSunrise);

		this.empty();
		this.addElement(new Container("sunrise", "SUNRISE : " + toTime(localSunrise)));
		this.addElement(new Container("sunset", "SUNSET : " + toTime(localSunset)));
		this.addElement(new Container("weather-container", this.getWeather()));
	};
}

class WeatherInfo extends DomElement {
	// Constructs a WeatherInfo object
	constructor (weather) {
		super('li', 'weather-info');
		console.log(weather);
		this.appendChild(new Container("weather-date", `${weather.dt_txt}: `));
		this.appendChild(new Container("weather-type", weather.weather[0].main));
		this.appendChild(new Container("weather-description", weather.weather[0].description));
		this.appendChild(new Container("weather-temp", `Temp: ${toCelsius(weather.main.temp)}, Min: ${toCelsius(weather.main.temp_min)}, Max: ${toCelsius(weather.main.temp_max)}`));
	};
}

/**
 * Creates an associative array containing the data stored in a given form
 *  such that the values can be indexed using the name of the input.
 *
 * @param {Form} The form for which name and value pairs should be extracted.
 */
function getValues(form) {
	let data = [];
	if (form.nodeName.toLowerCase() === "form") {
		let values = form.getElementsByTagName("input");
		for (let i = 0; i < values.length; i++) {
			if (values[i].type !== "button" && values[i].type !== "submit") {
				data[values[i].name] = values[i].value;
			}
		}
	}
	return data;
}

function toTime(date) {
	return date.toTimeString().split(" ")[0];
}

/**
 * Calculates the radius of a bounds Object by taking the distance between
 *  the northeast corner and the centre of the given bounds
 *
 * @param {Bounds} The bounds for which a radius should be found
 */
function getRadius(bounds) {
	let neLat = parseFloat(bounds.getNorthEast().lat()); 
	let neLng = parseFloat(bounds.getNorthEast().lng());
	let cLat = parseFloat(bounds.getCenter().lat());
	let cLng = parseFloat(bounds.getCenter().lng());

	let r = 6378.8 * Math.acos(
		Math.sin(toRadians(cLat)) * Math.sin(toRadians(neLat)) + 
		Math.cos(toRadians(cLat)) * Math.cos(toRadians(neLat)) * Math.cos(toRadians(neLng - cLng))
	);
	return r * 1000;
}

/**
 * Converts an angle from degrees to radians.
 *
 * @param {Float} The angle, theta, to be converted from degrees to radians.
 */
function toRadians(theta) {
	return theta * (Math.PI / 180);
}

/**
 * Converts a temperature from kelvin to celsius.
 *
 * @param {Float} The temperature, in kelvine, to be converted to degrees celsius.
 */
function toCelsius(kelvin) {
	return (kelvin -273.15).toFixed(2);
}

