<?php
	if (isset($_POST)) {
		// Get the lng/lat values from the POST data
		$lng = $_POST['lng'];
		$lat = $_POST['lat'];
		$appid = '8f5d38266ac43df53e1b5862ca0b3ccf';
		// Form the request URL
		$request = "api.openweathermap.org/data/2.5/forecast?lat={$lat}&lon={$lng}&APPID={$appid}";
		// Initiate a connection	
		$connection = curl_init($request);
		// Set parameters
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
		// Execute request	
		$response = curl_exec($connection);
		// Close connection	
		curl_close($connection);
		// Return response	
		echo $response;
	}
	
