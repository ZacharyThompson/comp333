/**
 * Various prototype functions for Window.
 */
Window.prototype.addOnload = function(action) {
	if (this.onload == null) this.onload = function() {};
	let oldAction = this.onload;
	this.onload = function () {
		oldAction();
		action();
	}
}

/**
 * Startup Sequence
 */
window.addOnload(function() {
	addReturnListener();
	addNavbarToggles();
	addCheckboxListener();
});

/**
 * The function to add toggles to checkbox elements. 
 * 
 */
function addCheckboxListener() {
	let nodes = document.getElementsByClassName("check-input");
	nodes.forEach(element => {
		toggleCheckbox(element);
	});
}

/**
 * The function to add return listeners to all elements with the return-listener class.
 * This will generally be used for text input to allow for the enter key to be
 * used to trigger a specified outcome. The outcome is specified under the 
 * onsubmit attribute.
 *
 */
function addReturnListener() {
	"use strict";
	let nodes = document.getElementsByClassName("return-listener");
	nodes.forEach(node => {
		node.addEventListener("keyup", event => {
            		if (event.key === "Enter") node.onsubmit();
		});
	});
}

/**
 * The function to add toggle listeners to all elements with the nav-link class.
 * This is used to toggle between navigation links which have been implemented
 * to allow for generic behaviour.
 *
 */
function addNavbarToggles() {
	"use strict";
	let links = document.getElementsByClassName("nav-link");
	links.forEach(a => {
		let click = a.onclick;
		a.onclick = function() {
			toggleNavbar(a);
			if (click != null && click !== a.onclick) click();
		};
        });
}

/**
 * The function used to toggle focus of a collapsable form. The function assumes
 * the form to be displayed is directly associated with the parameter passed. The
 * form to be displayed is isolated by comparing the id of all input-groups one
 * generation up.
 *
 * @param {Element} The button that is associated with the target form.
 */
function toggleForm(caller) {
	"use strict";
	let inputs = caller.parentElement.parentElement.children;
	inputs.forEachClass("input-group", child => {
		if (child.id === caller.name) {
			child.style.display = "block";	
		} else {
			child.style.display = "none";
		}
	});
}

/**
 * The function used to toggle all check-driven elements at the same HTML DOM
 * level as the checkbox that triggered the command.
 *
 * @param {Element} The control checkbox that is associated with sibling inputs to
 *     be disabled.
 */
function toggleCheckbox(checkbox) {
	"use strict";
	let inputs = checkbox.parentElement.children;
	inputs.forEachClass("check-driven", element => {
		element.disabled = checkbox.checked;
	});	
}

/**
 * The function to toggle between various tabs of the menu.
 *
 * @param {Element} Active. The anchor element that is requesting to be
 *    made the active anchor element within the navbar structure.
 */
function toggleNavbar(active) {
	"use strict";
	let inputs = active.parentElement.parentElement.children;
	inputs.forEachTag("li", list => {
		list.children.forEachTag("a", link => {
			link.classList.remove("active");
        	});
	});
	active.classList.add("active");
}

/**
 * Basic encapsulation of an Ajax request that handles generic request cases.
 *
 * @param {String} Method. The HTTP request method to be used.
 * @param {String} URL. The url to send the XHR request to.
 * @param {Boolean} Async. Whether the XHR request is asynchronous.
 * @param {String} Data. The body of data to be sent in the XHR request.
 * @param {Function} onSuccess. The function to be invoked if the XHR request
 *    was sucessful.
 * @param {Function} onError. The function to be invoked if the XHR request
 *    was not successful.
 */
function ajaxRequest(method, url, async, data, onSuccess, onError) {
	"use strict";
	let request = new XMLHttpRequest();
	request.open(method, url, async);
	
	if (method === "POST") {
		request.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	}

	request.onreadystatechange = function() {
		if (request.readyState === 4) {
			if (request.status === 200) {
				if (onSuccess != null) onSuccess(request);
			} else {
				if (onError != null) onError(request);
			}
		}
	};

	request.send(data);
}

/**
 * Function used to serialize a HTML form into a standard data String.
 * The String created is typically used with Ajax requests to send
 * data to the server.
 *
 * @param {Element} Form. The HTML form element to serialize.
 */
function serializeForm(form) {
	"use strict";
	let data = [];
	if (form.nodeName.toLowerCase() === "form") {
		let values = form.getElementsByTagName("input");
		for (let i = 0; i < values.length; i++) {
			if (values[i].type !== "button" && values[i].type !== "submit") {
				data.push(values[i].name + "=" + values[i].value);
			}
		}
	}
	return data.join("&");	
}

/**
 * Various protoype functions that are used to reduce the pesky
 * for loops required to iterate over HTMLCollections.
 */

HTMLCollection.prototype.forEach = function(action) {
    for (let i = 0; i < this.length; i++) {
        action(this[i]);
    }
};

HTMLCollection.prototype.forEachClass = function(className, action) {
	this.forEach(element => {
		if (element.classList.contains(className)) {
			action(element);
		}
	});
}

HTMLCollection.prototype.forEachTag = function(tagName, action) {
	this.forEach(element => {
		if (element.tagName.toUpperCase() === tagName.toUpperCase()) {
			action(element);
		}
	});
}

/**
 * Some basic prototyping on the Array Object to allow for remove and
 * contains operations.
 */

Array.prototype.indexOfObject = function(value) {
	for (let i = 0; i < this.length; i++) {
		if (typeof this[i].equals === 'function') {
			if (this[i].equals(value)) {
				return i;
			}
		}
	}
}

Array.prototype.removeObject = function(value) {
	let index = this.indexOfObject(value);
	if (index >= 0) {
		this.splice(index, 1);
	}
}

Array.prototype.containsObject = function(value) {
	return (this.indexofObject(value) >= 0);
}

Array.prototype.contains = function(value) {
	return (this.indexOf(value) >= 0);
}

Array.prototype.remove = function(value) {
	let index = this.indexOf(value);
	if (index >= 0) {
		this.splice(index, 1);
	}
}


