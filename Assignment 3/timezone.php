<?php
	if (isset($_POST)) {
		// Get the lng/lat values from the POST data
		$lng = $_POST['lng'];
		$lat = $_POST['lat'];
		// Get the current timestamp
		$date = date_create();
		$timestamp = date_timestamp_get($date);
		// Form the request URL
		$request = "https://maps.googleapis.com/maps/api/timezone/json?location={$lat},{$lng}&timestamp={$timestamp}";
		// Initiate a connection
		$connection = curl_init($request);
		// Set parameters	
		curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($connection, CURLOPT_SSL_VERIFYHOST,  2);
		// Execute request	
		$response = curl_exec($connection);
		// Close connection	
		curl_close($connection);
		// Return response	
		echo $response;
	}

